// pages/goods-detail/goods-detail.js
import goodsData from '../../data/goods-data'
Page({

  /**
   * Page initial data
   */
  data: {
    goods: {
      title: '',
      price: 0,
      mainPic: '',
      smallPic: '',
      details: [],
      createTime: 0,
      sale: 0
    },
    star: false,
    goodsIndex: -1
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    this.setData({
      goods: goodsData[options.goodsIndex],
      goodsIndex: options.goodsIndex
    })
  },


  onBackHome() {
    wx.redirectTo({
      url: '/pages/home/home'
    })
  },
  onStarChange() {
    this.setData({
      star: !this.data.star
    }, () => {
      wx.showToast({
        title: this.data.star? '已收藏' : '取消收藏',
        icon: this.data.star ? 'success' : 'none'
      })
    })
  },
  onBuyGoods(event) {
    wx.navigateTo({
      url: `/pages/order-message/order-message?goodsIndex=${this.data.goodsIndex}`
    })
  },
  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {

  }
})