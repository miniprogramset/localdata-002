// pages/goods-list/goods-lsit.js
import goodsData from '../../data/goods-data'
Page({

  /**
   * Page initial data
   */
  data: {
    sort: 0,
    goodsData: []
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    const goodsList = goodsData.sort((a,b) => a.price - b.price)
    this.setData({
      goodsData: goodsList
    })
  },

  onSortChange(event){
    const index = event.detail.index
    let goodsList = []
    switch(index) {
      case 0:
        goodsList = goodsData.sort((a,b) => b.price - a.price);
        this.setData({
          goodsData: goodsList
        })
        break;
      case 1:
        goodsList = goodsData.sort((a,b) => a.price - b.price);
        this.setData({
          goodsData: goodsList
        })
        break;
      case 2:
        goodsList = goodsData.sort((a,b) => a.sale - b.sale);
        this.setData({
          goodsData: goodsList
        })
        break;
    }
  },

  // 单击商品
  onClickCard(event) {
    wx.navigateTo({
      url: `/pages/goods-detail/goods-detail?goodsIndex=${event.currentTarget.dataset.index}`
    })
  },
  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {

  }
})