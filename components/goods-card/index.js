// const { Component } = require("XrFrame/xrFrameSystem");

// components/goods-card/index.js

Component({
	properties: {
		title: {
			type: String,
			default: "",
		},
		price: {
			type: Number,
			default: 0,
		},
		mainPic: {
			type: String,
			default: "",
		},
	},
	methods: {},
	lifetimes: {
		ready() {
			console.log("data", this.properties);
		},
	},
});
